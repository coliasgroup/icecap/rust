use super::icecap_impl as imp;

pub struct RwLock(imp::RwLock);

pub type MovableRwLock = RwLock;

unsafe impl Send for RwLock {}
unsafe impl Sync for RwLock {}

impl RwLock {
    pub const fn new() -> RwLock {
        RwLock(imp::RwLock::new())
    }

    #[inline]
    pub unsafe fn read(&self) {
        self.0.read()
    }

    #[inline]
    pub unsafe fn try_read(&self) -> bool {
        self.0.try_read()
    }

    #[inline]
    pub unsafe fn write(&self) {
        self.0.write()
    }

    #[inline]
    pub unsafe fn try_write(&self) -> bool {
        self.0.try_write()
    }

    #[inline]
    pub unsafe fn read_unlock(&self) {
        self.0.read_unlock()
    }

    #[inline]
    pub unsafe fn write_unlock(&self) {
        self.0.write_unlock()
    }
}
